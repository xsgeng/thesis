# 毕业论文

模板来自 https://github.com/mohuangrui/ucasthesis

pdf发布于 https://gitlab.com/xsgeng/thesis/-/releases

## 编译环境

- texlive-full 2019
    - XeLaTex

- Docker image
    - mirisbowring/texlive_ctan_full:2019

