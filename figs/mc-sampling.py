'''
atomic units
'''

from scipy.constants import  pi, alpha
from scipy.special import airy, kv
from scipy.integrate import quad
from scipy.interpolate import interp1d

from numpy import *
import matplotlib.pyplot as plt

hbar = 1
m = 1
c = 1 / alpha
e = 1

# def dPdδ(chi):
#     def core(delta):
#         z = ( delta / ( 1 - delta ) / chi )**(2/3)
#         g = 1 + delta**2/ 2/ ( 1 - delta )
#         fac = - alpha * m * c**2 / hbar
#         return fac * (quad(
#             lambda x : airy(x)[0],
#             z,
#             inf,
#         )[0] + g * 2 * airy(z)[1] / z)
    

#     return core


def dPdδApprox(chi):
    def core(delta):
        return e**2 * m * c / hbar**2 * 0.515 * chi**(2/3) * delta**(-2/3)
    
    return core


def Besselk(v):
    return lambda z : kv(v, z)


def dPdδ(chi):
    def core(delta):
        z = 2/3 / chi * delta / (1-delta)
        fac = e**2 * m * c / hbar**2 * 1.732/2/pi * chi * (1-delta)/delta
        return fac * (
            z * quad(
                Besselk(5/3),
                z,
                inf,
            )[0] + 1.5*delta*z * (z*Besselk(2/3)(z))
        )
    return core


chi = 0.1
delta = logspace(-3, 0, 100)

dt = 1
gamma = 1000

P = array(
    [dPdδ(chi)(delta_) for delta_ in delta]
) * dt / gamma
PApprox = array(
    [dPdδApprox(chi)(delta_) for delta_ in delta]
) * dt / gamma

fig, axes = plt.subplots(1, 2, figsize=(6.5, 2.5))

for ax in axes:
    ax.set_xlim(delta[0], delta[-1])
    ax.set_ymargin(0)
    
    ax.set_xscale('log')

    ax.set_xlabel(r'$\delta$')

# ax.set_ylabel(r'$\frac{d^2P}{d\delta d\tau}$')
# ax.set_ylabel(r'$\int d\delta \frac{d^2P}{d\delta d\tau}$')

axes[0].set_title('(a)', loc='left')
axes[1].set_title('(b)', loc='left')

linespec = dict(
    lw = 1,
    color = 'k',
)

'''ax1'''
ax = axes[0]

ax.plot(
    delta, P,
    **linespec,
    label = r'$P_{\chi_e}(\delta)$',
)

ax.fill_between(
    delta, 
    P,
    P*0,
    fc = 'k',
    alpha = 0.3,
)

ax.plot(
    delta, PApprox,
    **linespec,
    ls = '--',
    label = '$\sim \delta^{-2/3}$',
)

'''ax1'''
ax = axes[1]

P[1:] *= diff(delta)
Pcumsum = cumsum(P)

ax.plot(
    delta, Pcumsum,
    **linespec,
    label = r'$\int d\delta P_{\chi_e}(\delta)}$',
)

'''
interpolate annotations
'''
r2 = 0.4 * (nanmax(Pcumsum) - nanmin(Pcumsum)) + nanmin(Pcumsum)
delta_ = interp1d(Pcumsum[:-1], delta[:-1])(r2)

ax.plot(
    [delta[0], delta_],
    [r2, r2],
    **linespec,
    ls = '--',
)

ax.plot(
    [delta_, delta_],
    [nanmin(Pcumsum), r2],
    **linespec,
    ls = '--',
)

ax.scatter(
    delta[0], r2, 
    fc = 'none',
    ec = 'r',
    s = 10,
    clip_on = False,
)

ax.annotate(
    r'$r_1$',
    xy = (delta[0], r2),
    ha = 'right', va = 'bottom',
    color = 'r',
)

ax.scatter(
    delta_, nanmin(Pcumsum), 
    fc = 'none',
    ec = [0, 0.5, 1],
    s = 10,
    clip_on = False,
)

ax.annotate(
    r'$\delta$ of photon',
    xy = (delta_, nanmin(Pcumsum)),
    ha = 'left', va = 'bottom',
    color = [0, 0.5, 1],
)

axes[0].legend(loc = 'upper right')
axes[1].legend(loc = 'upper left')

fig.tight_layout()
fig.savefig(__file__.split('.')[0] + '.pdf')