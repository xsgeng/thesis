"""
script that creates the feynmann diagram of Thomson scattering.
"""
import matplotlib.pyplot as plt
from feynman import Diagram

fig, axes = plt.subplots(1, 2, figsize=(6, 2))

axes[0].set_title('(a)', loc='left')
axes[1].set_title('(b)', loc='left')

ax = axes[0]
ax.set_axis_off()
ax.set_xlim(-0.1, 1.1)
ax.set_ylim(-0.1, 1.1)

diagram = Diagram(ax)
e1 = diagram.vertex(xy = (0.0,  0.0), marker='')
e2 = diagram.vertex(xy = (0.25, 0.5), markersize=2 )
e3 = diagram.vertex(xy = (0.75, 0.5), markersize=2 )
e4 = diagram.vertex(xy = (1.0,  1.0), marker='')

e_in = diagram.line(e1, e2, linewidth=1)
diagram.line(e2, e3, linewidth=1)
e_out = diagram.line(e3, e4, linewidth=1)

pho1 = diagram.vertex(xy = (0.0,  1.0), marker='')
pho2 = diagram.vertex(xy = (1.0,  0.0), marker='')

pho_in = diagram.line(pho1, e2, style = "wiggly", linewidth=1)
pho_out = diagram.line(e3, pho2, style = "wiggly", linewidth=1)

e_in.text('e')
e_out.text('e')

diagram.text(0.2, 0.8, r'$\omega_0$', fontsize=12)
diagram.text(0.8, 0.2, r'$\omega_0$', fontsize=12)

diagram.plot()

ax = axes[1]
ax.set_axis_off()
ax.set_xlim(-0.1, 1.1)
ax.set_ylim(-0.1, 1.1)

diagram = Diagram(ax)
e1 = diagram.vertex(xy = (0.0,  0.0), marker='')
e2 = diagram.vertex(xy = (0.25, 0.5), markersize=2 )
e23 = diagram.vertex(xy = (0.5, 0.5), markersize=2 )
e3 = diagram.vertex(xy = (0.75, 0.5), markersize=2 )
e4 = diagram.vertex(xy = (1.0,  1.0), marker='')

e_in = diagram.line(e1, e2, linewidth=1)
diagram.line(e2, e23, linewidth=1)
diagram.line(e23, e3, linewidth=1, stroke="--", arrow=False)
e_out = diagram.line(e3, e4, linewidth=1)

pho1 = diagram.vertex(xy = (0.0,  1.0), marker='')
pho2 = diagram.vertex(xy = (0.25,  1.0), marker='')
pho3 = diagram.vertex(xy = (1.0,  0.0), marker='')

diagram.line(pho1, e2, style = "wiggly", linewidth=1)
diagram.line(pho2, e23, style = "wiggly", linewidth=1)
diagram.line(e3, pho3, style = "wiggly", linewidth=1)

e_in.text('e')
e_out.text('e')

diagram.text(0.2, 0.8, r'$\omega_0$', fontsize=12)
diagram.text(0.45, 0.8, r'$\omega_0$', fontsize=12)
diagram.text(0.8, 0.2, r'$n\omega_0$', fontsize=12)

diagram.plot()

fig.tight_layout()
fig.savefig(f"{__file__.split('.')[0]}.pdf")
