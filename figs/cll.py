'''
atomic units
'''

from scipy.constants import  pi, alpha
from scipy.special import airy
from scipy.integrate import quad

from numpy import *
import matplotlib.pyplot as plt

hbar = 1
m = 1
c = 1 / alpha
e = 1

def radPowerQED(chi):
    def dPdδ(delta):
        z = ( delta / ( 1 - delta ) / chi )**(2/3)
        g = 1 + delta**2/ 2/ ( 1 - delta )
        return quad(
            lambda x : airy(x)[0],
            z,
            inf,
        )[0] + g * 2 * airy(z)[1] / z

    power = quad(
        lambda delta : delta * dPdδ(delta),
        0,
        1,
    )[0]

    fac = -alpha * m * c**2 / hbar * m * c**2

    return fac*power # a.u.


def radPowerLL(chi):
    return 2/3 * e**2 * m**2 * c**3 / hbar**2 * chi**2


if __name__ == '__main__':
    fig, ax = plt.subplots(1, 1, figsize=(5, 3))
    axr = ax.twinx()

    ax.set_xmargin(0)
    ax.set_ymargin(0)

    ax.set_xscale('log')
    ax.set_yscale('log')

    ax.set_xlabel(r'$\chi_e$')
    ax.set_ylabel(r'radiation power / a.u.')

    axr.set_ylim(0, 1)

    chi = logspace(-3, 1, 100)

    PQED = asarray([radPowerQED(chi_e) for chi_e in chi])
    PLL  = radPowerLL(chi)

    h1, = ax.plot(
        chi,
        PQED,
        color='r',
        lw=1,
        ls='--',
        label='QED',
    )
    h2, = ax.plot(
        chi,
        PLL,
        color='k',
        lw=1,
        label='LL',
    )

    h3, = axr.plot(
        chi,
        PQED / PLL,
        color='r',
        lw=1,
        label=r'$g(\chi_e)$',
    )

    ax.legend(loc='center left')
    axr.legend(loc='center right')

    fig.tight_layout()
    fig.savefig('figs/cll.pdf')