using Statistics
# using PyPlot
using Plots
using LinearAlgebra

using vecpart

τ = 20
nt = 1500
dt = T/100
w0 = 2*λ

function initparticles(sz::Int, id::Int)
    x = ones(1, sz) * 0λ
    y = rand(1, sz) * 0
    z = rand(1, sz) * 0

    px = ones(1, sz) * 1000 * m * c
    py = zeros(1, sz)
    pz = zeros(1, sz)

    s = zeros(3, sz)

    part = Particles(
        id = id,
        r = [x; y; z],
        p = [px; py; pz],
        s = s,
        nt = nt,
        dt = dt,
    )
    return part
end
function laser!(em::EM, r::Array{Float64,2}, t::Float64)
    a0 = 80
    w₀ = w0
    E0 = m * c * k * c / e * a0

    x = @view r[1, :]
    y = @view r[2, :]
    z = @view r[3, :]

    ψ = k * x .+ k * c * t
    r = sqrt.(y .^ 2 + z .^ 2)

    waist = exp.(-r .^ 2 / w₀^2)
    prof = (0 .< ψ/2π .< τ) .* sin.(ψ / 2τ)

    # em.E[1, :] .= 0
    em.E[2, :] .= E0 .* sin.(ψ) .* prof .* waist
    # em.E[3, :] .= 0

    # em.B[1, :] .= 0
    # em.B[2, :] .= 0
    em.B[3, :] .= -em.E[2, :] / c
end

diags = Function[
    (part)->(@view part.r[1, :]),
    (part)->(@view part.r[2, :]),
]

@time x1, y1, = main(part=initparticles(1, 1), laser=laser!, diags=diags, rad=none, rr=false)
@time x2, y2, = main(part=initparticles(1, 1), laser=laser!, diags=diags, rad=LL)
@time x3, y3, = main(part=initparticles(20, 1), laser=laser!, diags=diags, rad=stochasticspin)

h = plot(
    x3[2:end, :]' / vecpart.λ,
    y3[2:end, :]' / vecpart.λ,
    line=1,
    color=:black,
    label="",
)
plot!(h,
    x3[1, :] / vecpart.λ,
    y3[1, :] / vecpart.λ,
    line=1,
    color=:black,
    label="QED",
)
plot!(h,
    x1' / vecpart.λ,
    y1' / vecpart.λ,
    line=2,
    color=:red,
    label="Lorentz",
)
plot!(h,
    x2' / vecpart.λ,
    y2' / vecpart.λ,
    line=2,
    color=RGB(0, 0.5, 1),
    label="LL",
)

fig = plot(h,
    size=(500, 300),
    framestyle=:box,
    legend=:topleft,
    xlim=(0, 10),
    ylim=(-0.5, 0.5),
    xlabel="x / λ",
    ylabel="y / λ",
)

savefig(fig, "random_traj.pdf")
