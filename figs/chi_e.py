# -*- coding: utf-8 -*-
"""
script that creates chi_e values for different laser intensity
and electron energy.
"""

from scipy.constants import  pi

import numpy as np
import matplotlib.pyplot as plt

wavelength = 0.8E-6

Intens = np.logspace(20, 25) # W/cm2
a = np.sqrt(Intens*1E4 / (2.73E10/wavelength**2))

E = np.linspace(0, 1000) # MeV
gamma = np.sqrt((E/0.511)**2 + 1)

fig, ax = plt.subplots(1, 1, figsize=(5, 3))
ax.set_xscale("log")

ax.set_xlim(1E20, 1E25)
ax.set_ylim(0, 1000)

ax.set_xlabel(r'Intensity $\rm W\cdot cm^{-2}$')
ax.set_ylabel(r'$\varepsilon_0$ MeV')

def chiplot(ax, chi, color, ls):
    gamma = chi / ( 6.07E-6 * 0.8E-6/wavelength * a )
    E = np.sqrt(gamma**2 - 1) * 0.511 # MeV
    
    l, = ax.plot(
        Intens, E, 
        color=color,
        ls=ls,
        )
    return l

def Rplot(ax, R, color, ls):
    gamma = R/(1/137 * 6.07E-6 * 0.8E-6/wavelength * a * a )
    E = gamma * 0.511 # MeV
    
    l, = ax.plot(
        Intens, E, 
        color=color,
        ls=ls,
        )
    return l

l1 = chiplot(ax, 0.1, 'k', '--')
l2 = chiplot(ax, 1, 'k', '-')
# l3 = Rplot(ax, 0.1, [1, 0, 0], '--')
# l4 = Rplot(ax, 1, [1, 0, 0], '-')

plt.legend([l1, l2],
           [
               r'$\chi_e=0.1$',
               r'$\chi_e=1$',
            #    r'$R=0.1$',
            #    r'$R=1$',
           ])

plt.tight_layout()
plt.show()
fig.savefig('chi_e.pdf')