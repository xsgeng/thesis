"""
script that creates the feynmann diagram of Compton scattering.
"""
import matplotlib.pyplot as plt
from feynman import Diagram

fig, ax = plt.subplots(1, 1, figsize=(5, 3))
ax.set_axis_off()

ax.set_ylim(0.25, 0.75)

diagram = Diagram(ax)
electronin1 = diagram.vertex(xy = (.1,.5))
electronin2 = diagram.vertex(xy = (.5,.5))

electronout1 = diagram.vertex(xy = (.5,.5))
electronout2 = diagram.vertex(xy = (.9,.3))

photon1 = diagram.vertex(xy = (.5,.5))
photon2 = diagram.vertex(xy = (.9,.7))

dressedin = diagram.line(electronin1, electronin2, stroke = 'double')
dressedout = diagram.line(electronout1, electronout2, stroke = 'double')
gamma = diagram.line(photon1, photon2, stroke = 'single', style = 'wiggly')

dressedin.text(r"$e^-$", fontsize = 20)
dressedout.text(r"$e^-$", t=0.55,y=-0.075, fontsize = 20)
gamma.text(r"$\gamma$", fontsize = 20)

diagram.plot()

fig.tight_layout()
fig.savefig(f"{__file__.split('.')[0]}.pdf")
