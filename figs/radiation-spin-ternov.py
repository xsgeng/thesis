'''
atomic units
'''

from scipy.constants import  pi, alpha
from scipy.special import kv
from scipy.integrate import quad

from numpy import *
import matplotlib.pyplot as plt

hbar = 1
m = 1
c = 1 / alpha
e = 1

def Besselk(v):
    return lambda z : kv(v, z)


def F(eta, chi, si, sf, rho):
    y = 4*chi / ( 3*eta * (eta - 2*chi) )
    xi = 3*eta / 2 
    fac = 9 * 3**0.5 * y / ( 16*pi * (1 + xi*y) )

    ret = 0
    if si*sf == 1:
        ret += (1 + xi*y/2)**2 * (
            quad(
                Besselk(5/3),
                y,
                inf
            )[0] + rho * kv(2/3, y)
        )
    
    if (rho == 1) & (si*sf == 1):
        ret += xi**2 * y**2 / 2 * quad(
            Besselk(1/3),
            y,
            inf,
        )[0] - sf * (2 + xi*y) * xi*y * kv(1/3, y)

    if si*sf == -1:
        ret += xi**2 * y**2 / 4 * (
            quad(
                Besselk(5/3),
                y,
                inf,
            )[0] - rho * kv(2/3, y)
        )
    
    if (rho == -1) & (si*sf == -1):
        ret += xi**2 * y**2 / 4 * (
            2 * quad(
                Besselk(1/3),
                y,
                inf,
            )[0] - 4 * sf * kv(1/3, y)
        ) * (1 - rho)/2

    return fac * ret


def dIdt(eta, si, sf, rho):
    fac = 2/3 * m**2 * c**3 * e**2 * eta**2 / hbar**2
    dydchi = lambda chi : 4 / ( 3 * eta * (eta - 2*chi) ) + 6*eta * 4*chi / (3*eta * (eta - 2*chi))**2

    ret = quad(
        lambda chi : dydchi(chi) * F(eta, chi, si, sf, rho),
        0,
        eta/2,
    )[0]

    return ret * fac


def dNdtau(eta, si, sf, rho):
    fac = 2/3 * m**2 * c**3 * e**2 * eta**2 / hbar**2
    dydchi = lambda chi : 4 / ( 3 * eta * (eta - 2*chi) ) + 6*eta * 4*chi / (3*eta * (eta - 2*chi))**2

    xi = 3*eta / 2

    y = lambda chi : 4*chi / ( 3*eta * (eta - 2*chi) )

    Ephoton_gamma = lambda chi : m * c**2 * xi * y(chi) / ( 1 + xi*y(chi) )

    ret = quad(
        lambda chi : dydchi(chi) * F(eta, chi, si, sf, rho)/Ephoton_gamma(chi),
        0,
        eta/2,
    )[0]

    return ret * fac


fig, ax = plt.subplots(1, 1, figsize=(5, 3))
 
ax.set_xlabel(r'$\chi_e$')
ax.set_ylabel(r'$P_{\mathrm{rad}}^{ss^\prime}/P_{\mathrm{rad}}$')

ax.set_xscale('log')

ax.set_xmargin(0)

n = 25
eta = logspace(-3, 1, n)

Prad = zeros((n, 2, 2, 2))
Nrad = zeros((n, 2, 2, 2))

for i, si in enumerate([-1, 1]):
    for j, sf in enumerate([-1, 1]):
        for k, rho in enumerate([-1, 1]):

            Prad[:, i, j, k] = array([
                dIdt(eta_, si, sf, rho) for eta_ in eta
            ])
            # Nrad[:, i, j, k] = array([
            #     dNdtau(eta_, si, sf, rho) for eta_ in eta
            # ])

Prad_ref = Prad.sum(axis=(1, 2, 3)) / 4 # 对rho平均
# Nrad_ref = Nrad.sum(axis=(1, 2, 3)) / 4 # 对rho平均

cases = [
    (1, 1, 'r',         '-',  r'$\uparrow\uparrow$'     ),
    (0, 0, [0, 0.5, 1], '-',  r'$\downarrow\downarrow$' ),
    (1, 0, 'r',         '--', r'$\uparrow\downarrow$'   ),
    (0, 1, [0, 0.5, 1], '--', r'$\downarrow\uparrow$'   ),
]

for i, j, c, ls, label in cases:
    ax.plot(
        eta,
        (Prad[:, i, j, 0] + Prad[:, i, j, 1])/2 / Prad_ref,
        lw=1,
        color=c,
        ls=ls,
        label=label,
    )
    # axes[1].plot(
    #     eta,
    #     (Nrad[:, i, j, 0] + Nrad[:, i, j, 1])/2 / Nrad_ref,
    #     lw=1,
    #     color=c,
    #     ls=ls,
    #     label=label,
    # )

ax.set_ylim(0)

ax.legend()

fig.tight_layout()
fig.savefig('figs/radiation-spin-ternov.pdf')