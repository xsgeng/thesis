\chapter{辐射反作用中的自旋效应}


\section{自旋对电子运动的影响}

\subsection{自旋对辐射的修正}

在对电子自发辐射极化的研究中，人们发现电子自旋处于反平行态时其辐射概率更高，
辐射光子的速率为\citep{bordovitsyn_synchrotron_1999}
（或参考\citep{del_sorbo_spin_2017,ternov_synchrotron_1995}）
\begin{equation}
    \label{eq:Nss}
    \frac{dN^{ss'}_\rho}{dt} = \frac{2}{3}\frac{e^2m^2c^3}{\hbar^2}\chi_e^2
    \int_0^{\chi_e/2} \mathrm{d}\chi_\gamma \frac{\mathrm{d}y}{\mathrm{d}\chi_\gamma}
    \frac{F(\eta, \chi, s, s', \rho)}{\hbar\omega}
\end{equation}
其中
$\chi_\gamma$是激光光子的量子参数
\begin{equation}
    \chi_\gamma = \frac{e\hbar^2}{2m^3c^4}|F^{\mu\nu}k_\mu|
\end{equation}
\begin{equation}
    y = \frac{4\chi_\gamma}{3\chi_e(\chi_e - 2\chi_\gamma)}
\end{equation}
$F(\eta, \chi, s, s', \rho)$是同步辐射函数
\begin{equation}
    \label{eq:Fss}
    \begin{split}
        F(\eta, \chi, s, s', \rho) =& \frac{9\sqrt{3}y}{16\pi(1+\xi y)^4}
        \times\\ &\bigg\langle
            \frac{1+ss'}{2}\bigg\{
                \Big(1+\frac{\xi y}{2}\Big)^2\Big[
                    \int_{y}^{\infty} K_{5/3}(x)\mathrm{d}x + \rho K_{2/3}(y)
                \Big] 
                \\ &+ \frac{1 + \rho}{2}\Big[
                    \frac{\xi^2y^2}{2}\int_{y}^{\infty} K_{1/3}(x)\mathrm{d}x 
                    - s'(2 + \xi y)\xi y K_{1/3}(y)
                \Big]
            \bigg\}
            \\ &+ \frac{1-ss'}{2}\frac{\xi^2y^2}{4}\bigg\{
                \int_{y}^{\infty} K_{5/3}(x)\mathrm{d}x - \rho K_{2/3}(y)
                \\ &+ \frac{1 - \rho}{2}\Big[
                    2\int_{y}^{\infty} K_{1/3}(x)\mathrm{d}x + 4s'K_{1/3}(y)
                \Big]
            \bigg\}
        \bigg\rangle
    \end{split}
\end{equation}
其中
$s,s' = \pm 1$是电子自旋的初态和末态，
$\rho$是产生光子的偏振态，
$\rho = 1$对应$\sigma$偏振（s偏），
$\rho = -1$对应$\pi$偏振（p偏）。
图\ref{fig:delsorbo-spinpol}给出了不同自旋态之间的跃迁概率
$\frac{dN^{ss'}_\rho}{dt}$，其中光子的极化态被平均处理，
所有的结果均归一到无极化电子的跃迁概率。
可以发现，随着量子效应增强，自旋反平行态的电子的辐射功率开始超过平行态的电子，
此时辐射反作用也开始增强，这意味着自旋处在反平行态的电子受到的辐射反作用力更大。
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{figs/radiation-spin-ternov.pdf}
    \bicaption{
        电子在不同自旋态间跃迁的辐射的概率与无极化电子的比值。
    }{
        Transition probability rate between different spin states divided by that of unpolarized electron. 
    }
    \label{fig:delsorbo-spinpol}
\end{figure}

若将(\ref{eq:Nss})式进行量子平均，如\ref{sec:量子修正}节所述，
便能够得到与自旋相关的半经典辐射功率，
\begin{equation}
    P_{\mathrm{rad}}^{s}
    = \int_0^1 \delta\gamma mc^2 \frac{\mathrm{d}^2N^{ss}_\rho}{dt \mathrm{d}\delta} \mathrm{d}\delta
\end{equation}
该辐射功率与经典辐射功率可定义为自旋相关的Gaunt系数$g^s(\chi_e)$，
我们便能够得到量子修正与自旋修正的辐射反作用力
\begin{equation}
    \label{eq:FR-spin}
    \Vector{F}_R^s = g^s(\chi_e) \Vector{F}_R
\end{equation}
\cite{sorbo_electron_2018}给出了$g^s(\chi_e)$的近似函数，
\begin{equation}
    \label{eq:gaunt-spin}
    \begin{split}
        g^s(\chi_e) \approx & \{
            1 + 2.54(\chi_e^2 - 1.28s\chi_e) \\
            & + (4.34 + 2.58s)(1 + \chi_e)
            \ln\left[ 
                1 + (1.98 + 0.11s)\chi_e
             \right]
        \}^{-2/3}
    \end{split}
\end{equation}

(\ref{eq:Fss})式能够用于电子自旋处于$\uparrow\downarrow$态的电子，
而对于更一般的情况，若电子处在部分极化态或者非极化态，或电子处在复杂的激光场中，
需要使用情况更广的理论模型，例如利用自旋密度矩阵方法来计算Compton散射\citep{seipt_theory_2018}。电子的自旋密度矩阵为
\begin{equation}
    \label{eq:rho}
    \rho = \frac{1 + \Vector{\sigma\cdot s}}{2}
\end{equation}
其中
$\Vector{\sigma}$是Pauli矩阵，
$\Vector{s}$是电子的自旋矢量。
\cite{seipt_theory_2018}给出了Volkov态电子Compton散射后的自旋密度矩阵
\begin{equation}
    \label{eq:spin-density-matrix}
    \rho = -\frac{\alpha mc^2}{2\hbar} \int \mathrm{d}\tau \mathrm{d}\delta \left( 
        \begin{matrix}
            \tilde{\mathcal{V}}_+ & \tilde{\mathcal{T}}^* \\
            \tilde{\mathcal{T}}   & \tilde{\mathcal{V}}_-
        \end{matrix}
     \right)
\end{equation}
其中
\begin{subequations}
    \begin{align}
        \tilde{\mathcal{V}}_\pm 
        & = (1\pm s_\zeta)\mathrm{Ai}_1(z)
        + (g\pm s_\zeta)\frac{2\mathrm{Ai}'(z)}{z}
        + \left(
            \delta s_\zeta \pm \frac{\delta}{1-\delta}
        \right) \frac{\mathrm{Ai(z)}}{\sqrt{z}}\\
        \tilde{\mathcal{T}}
        & = s_\kappa \left[
            (2g-1) \mathrm{Ai}_1(z) + g\frac{2\mathrm{Ai}'(z)}{z}
        \right] + i s_\eta \left[
            \mathrm{Ai}_1(z) + \frac{2\mathrm{Ai}'(z)}{z}
        \right]
    \end{align}
\end{subequations}
其中
$g = 1 + \frac{\delta^2}{2(1-\delta)}$，
$\delta = k^\mu k'_\mu / k^\mu p_\mu \approx \hbar\omega_\gamma/\gamma mc^2$，
$\mathrm{Ai}$是Airy函数，
$\mathrm{Ai}_1(z) = \int_z^\infty \mathrm{Ai}(x)\mathrm{d}x$，
$z = \left[\frac{\delta}{(1-\delta)\chi_e}\right]^{2/3}$。
密度矩阵的推导使用了一组基矢$(\Vector{\zeta, \eta, \kappa})$
作为电子自旋矢量的基矢，在场强满足$a_0 \ll 4700$时这组基矢可以近似为
电子静止系下的磁场、电场和波矢方向
$(\hat{\Vector{B}}',\hat{\Vector{E}}',\hat{\Vector{E}}'\times \hat{\Vector{B}}')$\citep{seipt_theory_2018}。

那么由密度矩阵给出的总辐射概率即为$\mathrm{Tr}(\rho)$，辐射的概率谱为
\begin{equation}
    \label{eq:Pzeta-seipt}
    \frac{\mathrm{d}^2P}{\mathrm{d}\delta \mathrm{d}\tau} 
    = \mathrm{Tr}\left(\frac{\mathrm{d}^2\rho}{\mathrm{d}\delta \mathrm{d}\tau}\right)
    = -\frac{\alpha m c^2}{\hbar}
    \left[
        \int_{z}^{\infty}{\mathrm{Ai}(x)\mathrm{d}x} 
        + g\frac{2\mathrm{Ai}'(z)}{z}
        + s_\zeta \delta \frac{\mathrm{Ai}(z)}{\sqrt{z}}
    \right]
\end{equation}
可以发现电子辐射光子的概率仅仅和电子自旋的$\zeta$分量相关，
即电子静止系下磁场方向的自旋分量，
也就是电子自旋态中反平行态的组分越多辐射的强度越高，受到的辐射反作用力也就越强。

\subsection{自旋-辐射耦合导致的偏转效应}

(\ref{eq:Fss})式和(\ref{eq:Fss})式
均表明自旋反向平行态的电子会受到更强的辐射反作用。
我们猜测电子的自旋态能够耦合辐射反作用，进而影响强场中电子的运动。
现在考虑一个相对简单的情况——一个极化的电子和平面波对撞过程，
由于激光场的周期性震荡，电子静止系下的磁场为
\begin{equation}
    \Vector{B}' 
    = \gamma \Vector{B} - \frac{\Vector{p}}{mc^2}
    - \frac{(\Vector{p\cdot B})\Vector{p}}{(\gamma + 1)mc^2}
\end{equation}
其方向与实验室系一致，$\Vector{B}' \sim \Vector{B}$，
因此电子的自旋态将会随着激光磁场的震荡而不断翻转，
自旋态对辐射的影响也随着平面波的震荡而被平均掉，
因此在平面波中难以观测到自旋对辐射以及电子运动的影响。

借助(\ref{eq:Pzeta-seipt})式与Monte-Carlo方法，
我们在计算中发现，极化电子在和聚焦后的激光对撞后，
相反自旋态的电子将会向相反方向偏转，如图\ref{fig:geng-spin-fig1}(a)所示，
电子的偏转效应并未如分析一般被激光场的周期震荡所平均。
这里的电子初始极化矢量与激光磁场方向一致，电子在激光中总是处在平行、反平行态。
对于初始能量为$\gamma_0 = 1000$的电子，我们统计了不同场强下电子偏转角的平均值，
如图\ref{fig:geng-spin-fig1}(b)所示，随着场强增加，
相反自旋态的电子散射后的偏转角差异越大。
图\ref{fig:geng-spin-fig1}(b)的右图为相反自旋态电子的角分布的差异，
由于辐射中的随机过程，单个电子的计算结果产生了较大的角分布，
而辐射过程对自旋态的依赖使得角分布出现了劈裂，向上和向下散射电子的数量并不相等。
电子在辐射过程中其自旋态有概率发生翻转，并且更倾向于反向平行态，
因此自旋翻转的存在将会一定程度上减小偏转角的差异，
如图\ref{fig:geng-spin-fig1}(b)中的虚线所示。

我们把相反自旋态电子偏转角的差异$\langle\theta^+\rangle - \langle\theta^-\rangle$
在$(a_0,\gamma_0)$空间进行了量化，
其中$\pm$上标表示电子极化沿着$\pm\hat{\Vector{y}}$方向，
结果如图\ref{fig:geng-spin-fig1}(c)和(d)所示，
图\ref{fig:geng-spin-fig1}(c)和(d)分别为无束腰和聚焦激光（$w_0 = 2\lambda$）的结果。
在没有激光束腰的影响时，偏转角的差异随着激光场强增加而线性增加；
而在聚焦的激光场中，偏转角产生了约10倍的增强，
并且在场强较强、电子能量较低时有更大的偏转角，
这一特性与电子在激光场中的有质动力散射一致。
此外我们还对比了SG效应产生的偏转，如图\ref{fig:geng-spin-fig1}(e)，
自旋通过耦合辐射反作用产生的偏转效应比SG效应要强约$10^4$倍，
这一效应为观测强场QED效应提供了一个全新的观测维度。
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{figs/geng-spin-fig1.pdf}
    \bicaptionwithtitle{相反自旋态电子和激光对撞后的偏转效应}{
        (a) 相反自旋态电子和激光对撞后的偏转效应。电子沿-z运动，激光沿z传播、沿x偏振。自旋平行（红色）、自旋反平行（蓝色）的电子向相反的方向偏转，而无自旋电子不发散偏转。
        (b) 初始能量为$\gamma_0 = 1000$的电子在不同场强下对撞后的偏转角，
        右侧为对撞后电子的角分布，对应$a_0 = 150$、半高全宽27fs、束腰$w_0 = 2\lambda$，灰色区域则是相反自旋态电子角分布的差值。左图的实线则是偏转角的平均值，虚线为考虑了自旋翻转效应的偏转角。
        (c-e) 不同$a_0,\gamma_0$所对应的相反自旋态电子的平均偏转角的差异。
        (c)是没有横向有质动力的情况，(d)是聚焦激光的情况，
        (e)是Stern-Gerlach效应导致的偏转角。
        出自\cite{geng_spin-dependent_2020}。
    }{Collision between a polarized electron and a strong laser pulse}{
        (a) Collision between a polarized electron and a strong laser pulse. The electron propagates along $-z$ direction; the laser propagates along $z$ direction with polarization along $x$. Electrons of different initial spin polarization, i.e. parallel (red) / anti-parallel (blue), get deflected to opposite direction due to spin-dependent radiation-reaction while the spin-free electron (gray) stays undeflected.
        (b) The deflection angle for different field strength with $\gamma_0 = 1000$. Curves on the right are the angular distribution of $\pm$ polarized electrons after collision with a laser of $a_0=150$, $T_{\mathrm{FWHM}}=27\mathrm{fs}$ and $w_0=2\lambda$; the gray area is the difference between spin $\pm$. Electron records are normalized in all cases.
        (c-e) Averaged deflection angle between spin $\pm$ electrons without transverse ponderomotive scattering (c,$w_0=\mathrm{Inf}$), with ponderomotive scattering (d,$w_0=2\lambda$) and for the Stern-Gerlach deflection (e) in the $(a_0, \gamma_0)$ parametric space. 
        From \cite{geng_spin-dependent_2020}.
    }
    \label{fig:geng-spin-fig1}
\end{figure}

\subsubsection{偏转机制}

为了解释自旋-辐射耦合导致的偏转效应，我们着眼于单个电子在平面波中的震荡过程，
如图\ref{fig:geng-spin-mechanism}(a)中的轨迹。
电子在激光场中做周期震荡的同时还受到辐射反作用力$\Vector{F}_r \sim \gamma^2$，
电子的能量也在不断下降，电子受到的辐射反作用力也会逐渐减小，
因此电子在上升和下降过程中沿着偏振方向受到的辐射反作用力不会像Lorentz力一样被抵消，
\begin{equation}
    \left|\int_{\mathrm{rise}} F_{r,x} dt \right|
    > \left|\int_{\mathrm{fall}} F_{r,x} dt \right|
\end{equation}
电子在半个激光周期中的动量将会产生微小的偏移，
我们用$\Delta p_x$表示辐射反作用导致的动量偏移，如图中的黑色箭头所示。
当我们考虑自旋对辐射的影响后，在磁场不变的半个周期内（如图中的红色$\times$区域）
自旋反向平行的电子受到的辐射反作用力更大（蓝色箭头），产生的动量偏移也越大，
而平行态的电子受到的辐射反作用力更小（红色箭头），即
\begin{equation}
    |\Delta p_x^-| > |\Delta p_x| > |\Delta p_x^+|
\end{equation}
这里$\pm$上标表示电子自旋矢量沿着$\pm\hat{\Vector{y}}$方向。
在磁场翻转的下半个周期（蓝色$\cdot$区域），动量变化的方向翻转，
电子的自旋态也发生了翻转，相反自旋态电子动量大小的变化同样发生了翻转
\begin{equation}
    |\Delta p_x^-| < |\Delta p_x| < |\Delta p_x^+|
\end{equation}
因此在一个激光周期中总是有
\begin{equation}
    \Delta p_x^- < \Delta p_x < \Delta p_x^+
\end{equation}
相反自旋态电子的偏转效应因此能够在激光的正负周期中得以累积。
以上过程中各个时刻的动量大小和方向以箭头的形式展示在了
图\ref{fig:geng-spin-mechanism}(b)和(c)中，
在$\times$区域，红色箭头比蓝色箭头更短，两者的平均值均为向下，
而在$\cdot$区域红色箭头更长，两者的平均值也发生了翻转，方向向上。
\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{figs/geng-spin-mechanism.pdf}
    \bicaptionwithtitle{电子和平顶激光对撞的对撞过程}{
        电子和平顶激光对撞的对撞过程。平顶区域为$-5<\psi/2\pi<5$。红色、蓝色表示$+/-$自旋态的电子，无自旋的电子为黑色。
        (a) 曲线是电子轨迹，箭头是辐射反作用在偏振方向产生的动量变化$\Delta p_x$。
        (b-c) 辐射反作用力在不同时间步产生的动量变化$\Delta\Vector{p}^{RR}$，``$\times$''与``$\cdot$''对应相邻的半周期。
        (d) 初始$+/-$自旋态电子的轨迹（红色、蓝色曲线）和半周期内的累积动量变化$\Delta p_x^\pm$。经典方程的结果为黑条，QED-MC的结果为红条。
        出自\cite{geng_spin-dependent_2020}。
    }{An electron collides with a flat-top laser pulse}{
        An electron collides with a flat-top laser pulse where the plateau region is $-5 < \psi / 2\pi < 5$ for spin $+/-$ (red/blue) and spin-free (black).
        (a) Electron trajectories (curves) and momentum change due to RR in $x$-direction in half-period ($\Delta p_x$ and arrows).
        (b-c) The variation of vector $\Delta\Vector{p}^{RR}$ along the collision time steps for adjacent half-periods (``$\times$'' for (b), ``$\cdot$'' for (c)).
        (d) The electron trajectories of initially $+/-$ polarized electron (red/blue lines) and the difference between $\Delta p_x^\pm$ accumulated in half-periods for classical radiation (black bars) and QED-MC radiation (red bars).
        From \cite{geng_spin-dependent_2020}.
    }
    \label{fig:geng-spin-mechanism}
\end{figure}

图\ref{fig:geng-spin-mechanism}(d)则是计算中统计的电子在激光半周期内的动量变化
与轨迹，其中黑色直方图为量子平均的半经典方程（(\ref{eq:FR-spin})式）的动量变化，
红色直方图为QED MC计算的动量变化，其中电子初始能量为$\gamma_0 = 1000$，
激光强度为$a_0 = 100$。
为了排除激光脉冲上升、下降沿对于计算的影响，我们使用了一个平顶激光包络，
在平顶区域电子的动量变化与上文的分析一致，
$+$态的电子相比于$-$态的电子在激光的各个半周期内总是向上偏转，
并未被激光的正负周期所平均，两者的轨迹也由于动量变化而逐渐劈裂，
未发生交错。
上一段基于电子经典运动轨迹分析了电子的动量变化，
而在量子效应较强时光子辐射的随机效应和随机的轨迹与经典轨迹有较大的不同，
如图\ref{fig:random_traj}所示。
而这里的统计结果中QED-MC计算和半经典方程计算的结果在动量变化上能够很好吻合
（图中的直方图），而决定电子偏转效应的正是其动量的相对变化，
因此半经典模型能够在忽略量子随机效应的情况下预估电子的偏转情况。
然而与之前分析不同的是，在激光脉冲的上升沿，
电子的动量变化与估计完全相反（$\Delta p_x^+ < \Delta p_x^-$）。
这是由于$\Vector{F}_R \sim \gamma^2 a_0^2$，
激光场强增强的同时电子受到的辐射反作用力也越来越大，
场强增加的效应超过了电子能量的损失的效应，
电子受到的辐射反作用力越来越大，
因此在图\ref{fig:geng-spin-mechanism}(a)中的$\Delta p_x$发生了反向，
使得图\ref{fig:geng-spin-mechanism}(d)中统计的动量变化也发生了反向。

从图\ref{fig:geng-spin-fig1}的(c)和(d)我们就能注意到，
聚焦的激光能够有效放大自旋-辐射耦合产生的偏转效应，
在图\ref{fig:geng-spin-ponderomotive}中我们就详细对比了激光束腰动量变化的影响。
同样的，这里我们使用了一束平顶激光，束腰分别为$\infty$和$2\lambda$，
相反自旋态电子的横向动量$p_x$在激光的平台区开始逐渐劈裂，如图中的黑线所示，
对于有束腰的情况，动量变化的差异比没有束腰的情况要高接近三个数量级。
这是由于在激光场中，
电子受到的横向有质动力为$\Vector{F}_{\mathrm{pond}}\sim \Vector{\nabla E}^2$，
当相反自旋态的电子由于辐射反作用力的差异而产生一个微小的劈裂$\delta x$后，
它们受到的有质动力的差异约为
\begin{equation}
    \delta F_{x,\mathrm{pond}} 
    \sim \frac{\partial}{\partial x} F_{x,\mathrm{pond}} \cdot \delta x
\end{equation}
对于高斯光束，在$|x| < w_0/2$的范围内，
\begin{equation}
    \frac{\partial }{\partial x}F_{x,\mathrm{pond}} \cdot \delta x
    \sim \frac{4}{w_0^2}\left(
        1 - \frac{4x^2}{w_0^2}
    \right) \exp \left(
        -\frac{2x^2}{w_0^2}
    \right) > 0
\end{equation}
$\delta F_{x,\mathrm{pond}}$则恒大于零，
$\delta x$将会被有质动力进一步放大，
而增大的$\delta x$也会进一步增大$\delta F_{x,\mathrm{pond}}$。
这一正反馈过程能够持续放大自旋-辐射耦合产生的偏转效应。
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{figs/geng-spin-ponderomotive.pdf}
    \bicaptionwithtitle{$\pm$自旋态的电子和平顶激光对撞的动量变化}{
        (a) 激光束腰为$w_0=2\lambda$；
        (b) 激光束腰为无穷大。
        黑线是动量$p_x^\pm$之间的差值。
        出自\cite{geng_spin-dependent_2020}。
    }{The momentum change of $\pm$ polarized electrons during collision with a flat-top laser}{
        (a) $w_0=2\lambda$ and 
        (b) infinite beam waist. 
        The difference between $p_x^\pm$ is shown by the black lines.
        From \cite{geng_spin-dependent_2020}.
    }
    \label{fig:geng-spin-ponderomotive}
\end{figure}

\subsubsection{电子束的极化效应}

在图\ref{fig:geng-spin-fig1}(a-b)中的偏转效应中我们已经知道，
相反自旋态的电子由于自旋-辐射耦合效应而向相反方向偏转，并且其空间分布并不重叠，
这意味着一束未极化的电子束在和强激光对撞后，
我们有可能观测到不同角度的电子有着不同的极化率。
为了评估可能出现的极化程度，我们计算了一束能量为$\gamma_0 = 1000$电子束和
$a_0 = 150$的激光的对撞过程，其中电子束能散为1\%，角发散度10 mrad
横向分布为Gaussian分布，半高全宽为$4\lambda$；
激光束采用了紧聚焦的形式\citep{salamin_electron_2002}，
脉宽的半高全宽为27 fs，束腰$w_0 = 2\lambda$。
对撞后电子的空间分布如图\ref{fig:geng-spin-bunch}(a)所示，
在线偏振光的作用下，被散射的电子在激光偏振方向的分布仍为对阵分布，
如图\ref{fig:geng-spin-bunch}(b)所示。当我们统计空间上的极化率时，
电子束沿着偏振方向有着不同的极化率，偏转角越大极化率越高，不再是对称分布，
如图\ref{fig:geng-spin-bunch}(c)中的黑线所示。
若这种极化效应能够在实验上进行观测，
我们将能够获得一种全新的探测强场QED的手段，即自旋效应。
这一极化效应在电子束尺寸、能散和发散角的影响下仍能够稳定存在，
如图\ref{fig:geng-spin-bunch}(c)中的红色点划线和蓝色虚线所示，
无能散、角发散的电子束和横向尺寸为$12\lambda$的电子束均给出了相近的极化分布。
由于有质动力能够有效放大这种偏转效应，
实验中紧聚焦的激光将能够更好的实现上述极化效应。
鉴于上述极化效应产生的极化率并不高，难以探测，
探测极化电子的偏转角将会是更好的选择。

\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{figs/geng-spin-bunch.pdf}
    \bicaptionwithtitle{激光、电子碰撞中的极化效应}{
        (a) 场强为$a_0 = 150$的线偏振激光和能量为$\gamma_0 = 1000$的未极化的电子束的对撞过程。电子的颜色表示电子的能量。右侧的颜色条是电子沿$\theta$角的空间极化率，红色、蓝色分别表示正、负极化。
        (b) 电子和激光对撞后的角分布，电子横向尺寸为$4\lambda$。
        (c) 电子自旋沿$y$方向的平均值的角分布。黑线表示电子的横向尺寸为$4\lambda$，能散为1\%，发散角为10 mrad，蓝色虚线表示更大的电子束尺寸$12\lambda$，红色点线表示横向尺寸为$4\lambda$、无能散、无发散角的结果。
        出自\cite{geng_spin-dependent_2020}。
    }{Polarization effects in the laser-electron collision}{
        (a) Collision between a linearly polarized laser pulse of $a_0=150$ and an unpolarized electron bunch of $\gamma_0=1000$. The color of the electrons represents the energy of the electrons. The color bar on the right represents the spatial polarization along $\theta$. The red/blue end represents positive/negative polarization. 
        (b) Angular distribution after collision for electron transverse width of $4\lambda$ (gray area).
        (c) The angular distribution of the mean value of spin along $y$-axis for small transverse size of  $4\lambda$ with 1\% energy spread and 10 mrad angular divergence (black-solid), large transverse size of $12\lambda$ (blue-dashed) and transverse size of $4\lambda$ without energy and angular divergence (red-dot-dashed). 
        From \cite{geng_spin-dependent_2020}.
    }
    \label{fig:geng-spin-bunch}
\end{figure}


\section{辐射对自旋的影响\label{sec:辐射对自旋的影响}}

\subsection{自旋态的演化}

上一节我们主要将注意力放在电子自旋态对于辐射概率的影响，
并发现电子照自旋态能够耦合辐射反作用力，使电子在激光场中产生额外的偏转。
接下来我们将专注于辐射过程中自旋态的变化。
Sokolov-Ternov (ST)效应（\ref{sec:ST}节）表明，
电子在辐射光子时其自旋态也会有概率发生变化，
电子从自旋平行态跃迁到反平行态的概率大于反平行态到平行态的跃迁概率，
即$P^{\uparrow\downarrow} > P^{\downarrow\uparrow}$。
在ST效应中，背景电磁场为静磁场，
这一过程可以简化为电子自旋态的平行、反平行态间的跃迁，或自旋翻转，
如图\ref{fig:spin-models}(a)的过程，
该过程的辐射概率与电子自旋态的跃迁概率由(\ref{eq:Nss})式给出。

对于强激光场中的电子，电子感受到的磁场大小和方向会在激光周期尺度内发生巨大的变化，
与ST效应中的静磁场大相径庭，因此需要新的方法来处理强激光场中的自旋演化过程。
ST效应是实验上已经观测到的自旋演化过程，如图\ref{fig:st}，
因此用于描述激光场中自旋变化的新模型需要在静磁场中复现出ST效应的结果。
\cite{li_ultrarelativistic_2019}首先尝试了
图\ref{fig:spin-models}(b)中的模型：
电子在辐射高能光子时电子的自旋矢量将会直接投影到电子静止系下的磁场方向，
即量子化轴的方向，
电子自旋的末态由\citep{li_ultrarelativistic_2019}中的(1)式
通过Monte-Carlo过程决定，
电子自旋末态处在上、下态的概率由跃迁到上、下态的概率的相对大小决定，
即$\frac{P^{\Vector{s}\uparrow}}{P^{\Vector{s}\uparrow} + P^{\Vector{s}\downarrow}}$、
$\frac{P^{\Vector{s}\downarrow}}{P^{\Vector{s}\uparrow} + P^{\Vector{s}\downarrow}}$。
这种方案对于任意自旋矢量的电子均能在光子辐射的过程中复现ST效应。
随后，对于更一般的情况，量子化轴的选取与所计算的具体问题相关，
例如对于初始纵向极化的电子，
量子化轴可以选择为电子动量的方向\citep{li_polarized_2020}。

从单电子的角度出发，以上模型能够在特定情况下计算电子在辐射过程中的自旋演化。
而对于一定相空间范围内的大量电子，其极化矢量并不会在光子辐射时发生剧烈变化，
其极化矢量应与ST效应相一致，极化矢量在不断辐射光子的过程中
从任意初态（包括部分极化和未极化的情况）连续变化到完全极化的状态
$\frac{P^{\downarrow\uparrow} - P^{\uparrow\downarrow} }{ P^{\downarrow\uparrow} + P^{\uparrow\downarrow} }$，
如图\ref{fig:spin-models}(c)所描述的过程。
从这一特性出发，接下来我们将给出了对于一般情况下（非静磁场）的ST效应。
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{figs/spin-models.png}
    \bicaptionwithtitle{辐射过程中的自旋变化}{
        (a)自旋在上下态之间的跃迁。
        (b)自旋投影到量子化轴。
        (c)自旋在辐射中逐渐靠近量子化轴。
    }{Spin evolution during photon emission}{
        (a) Transition between spin-up/-down states.
        (b) Projection onto the spin quantization axis.
        (c) The spin vector tilts towards the spin quantization axis during radiation.
    }
    \label{fig:spin-models}
\end{figure}

\subsection{强激光场中的辐射极化效应}

在ST效应中，电子沿着磁场的反方向逐渐建立极化，
这是由于电子向下态的跃迁概率更高，
因此极化建立的过程可以由速率方程（(\ref{eq:Nup-rate})式）所描述，
即$\frac{\mathrm{d}}{\mathrm{d} t} N^\uparrow = P^{\downarrow\uparrow} N^\downarrow - P^{\uparrow\downarrow} N^\uparrow$。
$N^\uparrow$和$N^\downarrow$是当前时刻处在自旋上、下态的电子数目，
这里的上下态定义在磁场方向，
$P^{\uparrow\downarrow}$和$P^{\downarrow\uparrow}$
则是上下态之间的跃迁速率。
将电子上下态的数目替换为极化率
\begin{equation}
    \bar{s}
    \equiv Polarization
    = \frac{N^\uparrow-N^\downarrow}{N^\uparrow+N^\downarrow}
\end{equation}
便能得到极化率的速率方程
\begin{equation}
    \label{eq:Pol-rate}
    \frac{\mathrm{d}}{\mathrm{d}t} \bar{s} + (A+B)\bar{s} 
    = A - B
\end{equation}
这里我们用$A = P^{\downarrow\uparrow}$和$B = P^{\uparrow\downarrow}$
来简化之后的描述。(\ref{eq:Pol-rate})式的解为
\begin{equation}
    \label{eq:Pol(t)}
    \bar{s}(t) = \frac{A-B}{A+B}\left[
        1 - \exp(-t/\tau)
    \right] + \bar{s}_0\exp(-t/\tau)
\end{equation}
其中
$\tau = \frac{1}{A+B}$是极化的时间尺度，
$\bar{s}_0$是极化的初始值。
从(\ref{eq:Pol(t)})式可以看出，电子在辐射过程中总会沿着$(A-B)/(A+B)$方向建立极化，
$(A-B)/(A+B)$则是极化所能达到的最大值；而电子的初始极化$\bar{s}_0$则只会发生退极化过程。

原则上(\ref{eq:Pol(t)})式可以用于计算任意量子化轴的计算，
而不仅限于ST效应中的静磁场方向；前提条件是我们有电子沿任意轴翻转的跃迁速率，
即沿任意方向的$A$和$B$，
因此(\ref{eq:Pol(t)})式所对应的方法我们在这里称作一般情况下的ST效应
（generalized Sokolov-Ternov effect, GST）。

(\ref{eq:Pol(t)})式所需的自旋翻转概率可以从
\cite{seipt_theory_2018}给出的自旋密度矩阵，
即(\ref{eq:spin-density-matrix})式来进行推导，
借助极化密度矩阵我们能够计算电子自旋矢量沿任意轴的翻转概率。
为了计算自旋矢量的变化，我们需要选取一组正交完备的基矢，
这里我们使用与\cite{seipt_theory_2018}一致的基矢，
即电子静止系下的磁场、电场和波矢方向$(\Vector{\zeta, \eta, \kappa})$。
电子从$\rho$到$\rho'$的跃迁概率为$\mathrm{Tr}(\rho\rho')$，
那么$(\Vector{\zeta, \eta, \kappa})$三个方向上的跃迁概率则为
\begin{subequations}
    \label{eq:ABs}
    \begin{align}
        A^\zeta &
        = \mathrm{Tr}[\rho(\Vector{\zeta}) \cdot \rho(-\Vector{\zeta})]
        = -\frac{\alpha mc^2}{2\hbar} \int_0^1 \frac{\delta^2}{1-\delta}\left[
            \frac{{\mathrm{Ai}}^\prime (z) }{z}+\frac{\mathrm{Ai} (t)}{\sqrt z}
        \right] \mathrm{d}\delta \\
        B^\zeta &
        = \mathrm{Tr}[\rho(-\Vector{\zeta}) \cdot \rho(\Vector{\zeta})]
        = -\frac{\alpha mc^2}{2\hbar}
        \int_0^1 \frac{\delta^2}{1-\delta}\left[
            \frac{{\mathrm{Ai}}^\prime (z) }{z}-\frac{\mathrm{Ai} (z) }{\sqrt z}
        \right] \mathrm{d}\delta \\
        A^\eta &
        = \mathrm{Tr}[\rho(\Vector{\eta}) \cdot \rho(-\Vector{\eta})]
        = -\frac{\alpha mc^2}{2\hbar}
        \int_0^1 \frac{\delta^2}{1-\delta}\left[
            \frac{{\mathrm{Ai}}^\prime (z) }{z}
        \right] \mathrm{d}\delta \\
        B^\eta &
        = \mathrm{Tr}[\rho(-\Vector{\eta}) \cdot \rho(\Vector{\eta})]
        = A^\eta \\
        A^\kappa &
        = \mathrm{Tr}[\rho(\Vector{\kappa}) \cdot \rho(-\Vector{\kappa})]
        = -\frac{\alpha mc^2}{2\hbar}
        \int_0^1 \frac{\delta^2}{1-\delta}\left[
            \frac{2\mathrm{Ai}^\prime (z) }{z} - \mathrm{Ai}_1 (t)
        \right] \mathrm{d}\delta \\
        B^\kappa &
        = \mathrm{Tr}[\rho(-\Vector{\kappa}) \cdot \rho(\Vector{\kappa})]
        = A^\kappa
    \end{align}
\end{subequations}
由于密度矩阵(\ref{eq:spin-density-matrix})式为LCFA的结果，
上式同样需要满足LCFA条件(\ref{eq:LCFA})式。为了更直观的理解(\ref{eq:LCFA})式，
我们将不同$\chi_e$对应的最大极化率$(A^\zeta - B^\zeta) / (A^\zeta + B^\zeta)$和
极化时间尺度$1/(A^\zeta + B^\zeta)$展示在了图\ref{fig:geng-GST-ABs}(a)中，
其中
$(A^\zeta - B^\zeta) / (A^\zeta + B^\zeta) \approx -0.924$
是经典极限下ST效应所对应的最大极化率，
$\tau^{\mathrm{S-T}} = \frac{8\sqrt{3}}{15} \frac{\hbar^2}{mce^2} \gamma \chi_e^{-3}$\citep{ternov_synchrotron_1995}是ST效应的极化时间尺度。
在磁场方向$\mathrm{\zeta}$上，
最大极化率和极化时间均能在$\chi_e \ll 1$时复现出ST效应。
由于在$\eta$和$\kappa$方向上$A = B$，电子无法在垂直磁场方向建立极化，
该方向上的任何极化率只会发生退极化，这与\cite{seipt_theory_2018}的结论一致。
此外，图\ref{fig:geng-GST-ABs}(a)表明，随着$\chi_e$增加，极化时间快速下降，
而当$\chi_e > 0.1$时极化时间开始趋于平缓，
另一方面电子所能达到的最大极化率也开始快速下降。
图\ref{fig:geng-GST-ABs}(b)为(\ref{eq:ABs})式在不同$\chi_e$下的具体变化，
$A,B$均在$\chi_e$增加的过程中快速增加，而$A^\zeta$和$B^\zeta$的增速不一致，
如图\ref{fig:geng-GST-ABs}(b)中的红线，使得最大极化率
$(A^\zeta - B^\zeta) / (A^\zeta + B^\zeta) 
= (A^\zeta/B^\zeta - 1) / (A^\zeta/B^\zeta + 1)$下降。
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{figs/geng-GST-ABs.pdf}
    \bicaptionwithtitle{$A$、$B$系数}{
        (a)（左轴）最大极化率$(A^\zeta - B^\zeta) / (A^\zeta + B^\zeta)$和（右轴）极化时间$1/(A^\zeta + B^\zeta)$在不同$\chi_e$下的值，其中极化时间由$\gamma$进行了归一化。
        (b)（左轴）(\ref{eq:ABs})式中$A,B$的变化和（右轴）$A^\zeta/B^\zeta$。
        出自\cite{geng_generalizing_2020}。
    }{Factor $A$s and $B$s}{
        (a) (left axis) The polarization limit $(A^\zeta - B^\zeta) / (A^\zeta + B^\zeta)$ and (right
        axis) the polarization time $1/(A^\zeta + B^\zeta)$ scaled by $\gamma$.
        (b) (left axis) The $A$s and $B$s in Eqs. (\ref{eq:ABs}) scaled by $\gamma$ and (right axis) $A^\zeta/B^\zeta$.
        From \cite{geng_generalizing_2020}.
    }
    \label{fig:geng-GST-ABs}
\end{figure}

接下来我们计算了电子与线偏振激光对撞的过程，
并对比了上述几种自旋处理方式（图\ref{fig:spin-models}）的差别。
在计算辐射时自旋投影到磁场方向的情况时，我们采用了Monte-Carlo方法，
对于GST模型，即(\ref{eq:Pol(t)})式，我们赋予单个电子一个平均极化矢量，
并计算该极化矢量在激光场中的变化。
为了对比自旋的演化，我们在计算中没有包含辐射反作用，
这是由于随机辐射反作用在量子效应较强时，
随机辐射过程产生的结果与GST这种量子平均的算法有较大的不同。
计算中电子的初始能量为$\gamma_0 = 1000$，
激光强度为$a_0 = 150$，其包络为
\begin{equation}
    h(\psi) = a_0 \exp\left(
        -\frac{x^2 + y^2}{w_0^2}
    \right) \cos(\psi) \cos^2(\frac{\psi}{2N_\tau})
\end{equation}
其中
$w_0 = 5\lambda$是束腰的大小，
$N_\tau = 20$代表激光的脉宽，
波长为$\lambda = 800\mathrm{nm}$。
激光的偏振方向为$x$，磁场的偏振方向为$y$，
因此我们计算了初始极化为$\langle\Vector{s}\rangle_y = 1$的电子的极化率的演化，
结果如图\ref{fig:geng-GST-sy-LP}(a)所示。
三种模型在电子自旋态仅为上下态的情况下能够达成一致，
电子在激光场中其自旋态随着激光周期变化而发生翻转，
电子的极化率也因此在相邻的半周期内发生极化和退极化过程。
对于初始纵向极化的电子$\langle\Vector{s}\rangle_z = 1$，
ST效应所对应的模型并不适用于该情况，
其横向极化的变化展示在了图\ref{fig:geng-GST-sy-LP}(b)中，
GST模型与投影模型能够较好地相互吻合，这是由于电子在经历少量几个激光周期后，
辐射了较多的光子，使得几乎所有电子自旋矢量都投影到了磁场方向，
从而回到了电子仅处于上下态的情况。
\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{figs/geng-GST-sy-LP.pdf}
    \bicaptionwithtitle{电子和线偏振激光对撞中横向极化率的变化}{
        (a) 初始极化为$\langle\Vector{s}\rangle_y = 1$电子的极化率的演化。
        (b) 初始极化为$\langle\Vector{s}\rangle_z = 1$电子的极化率的演化。
        出自\cite{geng_generalizing_2020}。
    }{Electron polarization evolution during collision with linearly polarized laser pulse}{
        (a) Initial polarization of $\langle\Vector{s}\rangle_y = 1$ and 
        (b) initial polarization of $\langle\Vector{s}\rangle_z = 1$.
        From \cite{geng_generalizing_2020}.
    }
    \label{fig:geng-GST-sy-LP}
\end{figure}
