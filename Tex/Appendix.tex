\chapter{vecpart.jl\label{app:vecpart}}

vecpart.jl是Julia语言的程序包，用于快速计算大量试探粒子与电磁场的相互作用，
发布于https://gitlab.com/xsgeng/vecpart.jl。
Julia语言的运算速度接近C语言，并且保留了脚本语言的便利性。
Julia语言能够通过向量化操作快速进行矩阵运算；相比于循环操作，
向量化操作使得CPU能够借助特殊的指令集同时进行多个数字的运算，
具体的操作数取决于CPU内部寄存器的位宽和其支持的指令集。
利用MPI等方式可以实现多进程计算，计算性能能够线性拓展。

vecpart.jl将所有粒子的坐标、动量、自旋等信息均排列为内存空间中连续的向量以利用
Julia语言的自动向量化操作，尽可能地提高运算效率。
因此对于一个Julia进程，同时处理尽可能多的粒子数能够有效提高运算效率。
图\ref{fig:vecpart-speed-scaling}是不同粒子数所对应的每个粒子的平均运算时间，
数值越低意味着单位时间内能够处理的粒子数越高。
\begin{figure}[H]
    \centering
    \includegraphics[width=5in]{figs/vecpart-speed-scaling.pdf}
    \bicaption{
        单个Julia进程对于不同粒子数的运算时间的变化（越低意味着越快）。
    }{
        The time consumption of iteration per particle for different number of particles (the lower the faster).
    }
    \label{fig:vecpart-speed-scaling}
\end{figure}
对于我们所使用的计算机，$npart$在$[10^3, 10^5]$区间均能达到较高的运算速度，
$\approx 500\mathrm{ns}$每个粒子。对于$npart > 10^5$，速度出现了下降，
这是由于计算过程中使用的坐标、动量、自旋等向量的长度大大增加，
而运算过程中不可避免得会产生与它们长度相等的中间变量，从而产生了较高的内存开销，
此时内存速度成为了运算速度的短板。

为了保证速度与精确性，vecpart.jl使用Boris方法\citep{boris_relativistic_1970}
来对粒子动量进行积分。对于一般的积分方法，如矩形积分，一个时间步长所产生的动量变化为
\begin{equation}
    \Delta\Vector{p} = q(\Vector{E + v \times B})\Delta t
\end{equation}
其中磁场并不做功，但磁场引起的动量变化为$q(\Vector{v \times B})\Delta t$，
增加了动量的大小，引起了能量不守恒的问题。
因此\cite{boris_relativistic_1970}提出了三步积分的方法来抑制这一问题：\\
1. 在电场作用下动量产生半个时间步长的变化$\frac{q\Vector{E}\Delta t}{2}$，
动量从$\Vector{p}^n$变为
$\Vector{p}^- = \Vector{p}^n + \frac{q\Vector{E}\Delta t}{2}$；\\
2. $\Vector{p}^-$在磁场的作用下做$q\Vector{v\times B}\Delta t$的旋转，动量变为$\Vector{p}^+$；\\
3. 在电场作用下动量产生剩下半个时间步长的变化$\frac{q\Vector{E}\Delta t}{2}$，
动量变为$\Vector{p}^{n+1} = \Vector{p}^+ + \frac{q\Vector{E}\Delta t}{2}$。\\
关于Boris方法和其他积分方法还可参考\cite{qin_why_2013,ripperda_comprehensive_2018}。

vecpart.jl包含了电子自旋矢量以及T-BMT方程，即(\ref{eq:TBMT})式。
由于Boris方法对动量的积分分为电场和磁场部分，而进动方程只包含自旋矢量的旋转操作，
并不改变自旋矢量的长度，因此自旋进动过程与Boris方法的磁场旋转部分同步发生。
因此电子进动轴要基于Boris方法第二步$\Vector{p}^-$的结果进行计算，即
$\Vector{\Omega}_s^-$，
进动量为$\frac{e}{mc}\Vector{s \times \Omega}_s^- \Delta t$。

vecpart.jl最初的开发目的是测试新的辐射模型，
\ref{sec:QEDMC}节中介绍了光子辐射Monte-Carlo算法，
可以发现辐射谱的计算与采样涉及Airy函数或Bessel函数等特殊函数的积分，
这类积分计算极为耗时，因此这类特殊函数的积分结果被保存在静态表格中用于快速差值计算。
借助元编程，vecpart.jl能够方便而快速地生成任意积分表格并直接存储在代码中。

\chapter{lw.jl\label{sec:lw.jl}}

lw.jl是Julia语言的程序包，用于快速计算大量电子在定义的观测方向的辐射谱，
发布于https://gitlab.com/xsgeng/lw.jl。
lw.jl利用FFTW\citep{FFTW05}计算电子辐射的电磁场在观测点处的频谱，
多线程的使用进一步加速了计算，如使用MPI等方式可以实现跨节点分布式计算，进一步提升计算性能。

在一般情况下，计算过程中粒子的坐标、动量定义在均匀的时间网格上，
但刻电子在这些时刻所辐射出的电磁场在到达远处的观测点的时间却是不均匀的：
对于一个无穷远的观测点，
电子$\Vector{r}$到观测点的矢量$\Vector{R}$可以近似为原点到观测点的矢量$\Vector{x}$，$R$便能近似为$x - \Vector{n\cdot r}$，
那么在观测点处观察到的时间便是
\begin{equation}
    t_{\mathrm{retard}} = t - \frac{x - \Vector{n\cdot r}}{c}，
\end{equation}
见(\ref{eq:retard-time})式，其中$x/c$是一个常量，可以略去。
我们的目标是对电子辐射出的电磁场进行Fourier变换，进而分析电子辐射的频谱，
而快速Fourier变换（FFT）\citep{FFTW05}是一种成熟而快速的Fourier变换算法。
但是在不均匀网格上定义的振幅将无法进行FFT操作，因此lw.jl在进行FFT之前先将nt个数据点线性插值到nt\_detector个均匀网格上，随后再进行FFT操作。

接下来我们便可以计算辐射场的能谱$\frac{\Vector{d}^2 I}{\Vector{d}\Omega \Vector{d}\omega}$，
即单位立体角、单位圆频率范围内的电子能量，详细过程可参考\cite{richard_electromagnetic_2012}。
辐射场的能流密度为Poynting矢量，即单位时间内流过单位面积的能量
\begin{equation}
    \Vector{S} = \frac{c^2}{4\pi} \Vector{E\times B} = \frac{c}{4\pi} \Vector{E}^2 \Vector{n}
\end{equation}
那么单位时间内流过单位立体角的能量为
\begin{equation}
    \mathrm{d}P = (\Vector{S\cdot n}) R^2\mathrm{d}\Omega = \frac{c}{4\pi} R^2\Vector{E}^2\mathrm{d}\Omega
\end{equation}
注意，我们只考虑辐射场(\ref{eq:LW})式的第二项，辐射场强度$\sim R^{-1}式$，这里的$R$便可以消掉。那么
\begin{equation}
    \frac{\mathrm{d}I}{\mathrm{d}\Omega} = \int_{-\infty}^{+\infty} \frac{c}{4\pi} R^2\Vector{E}^2 \mathrm{d}t
\end{equation}
对于一个辐射谱，它在频域的积分和其时域的积分（即能量）相等，那么
\begin{equation}
    \frac{\mathrm{d}I}{\mathrm{d}\Omega} 
    = \int_{-\infty}^{+\infty} \mathrm{d}t 
    \left(\frac{1}{\sqrt{2\pi}}\right)^2
    \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty} \mathrm{d}\omega_1 \mathrm{d}\omega_2 
    \mathcal{F}(\omega_1) \mathcal{F}^*(\omega_2) \exp[i(\omega_2-\omega_1)t]
\end{equation}
其中$\mathcal{F}(\omega)$是$\sqrt{\frac{c}{4\pi}}R\Vector{E}$的Fourier变换
\begin{equation}
    \mathcal{F}(\omega) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty}
    \sqrt{\frac{c}{4\pi}}R\Vector{E} \exp(i \omega t) \mathrm{d}t
\end{equation}
那么，借助$\int \exp[i(\omega_2-\omega_1)t] \mathrm{d}t = 2\pi\delta(\omega_2 - \omega_1)$，可以得到
\begin{equation}
    \frac{\mathrm{d}I}{\mathrm{d}\Omega} 
    = \int_{-\infty}^{+\infty} |\mathcal{F}(\omega)|^2 \mathrm{d}\omega
    = \int_{0}^{+\infty} 2|\mathcal{F}(\omega)|^2 \mathrm{d}\omega
    = \int_{0}^{+\infty} \frac{\mathrm{d}^2I}{\mathrm{d}\Omega\mathrm{d}\omega} \mathrm{d}\omega
\end{equation}
即
\begin{equation}
    \frac{\mathrm{d}^2I}{\mathrm{d}\Omega\mathrm{d}\omega} 
    = 2|\mathcal{F}(\omega)|^2
\end{equation}
替换$\mathcal{F}(\omega)$为FFT过程便能够快速计算辐射能谱。